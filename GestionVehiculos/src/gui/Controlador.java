package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import base.Coche;

public class Controlador implements ActionListener{

	private Vista vista;
	private Modelo modelo;
	
	public Controlador(Vista vista, Modelo modelo) {
		this.vista = vista;
		this.modelo = modelo;
		
		addActionListeners(this);
	}

	/**
	 * Metodo que vincula el objeto listener (esta propia clase)
	 * con los componentes gr�ficos sobre los que quiero escucar
	 * eventos (botones en este caso)
	 * 
	 * @param listener objeto que actua como ActionListener
	 */
	private void addActionListeners(ActionListener listener) {
		vista.btnEliminarCoche.addActionListener(listener);
		vista.btnNuevoCoche.addActionListener(listener);
	}

	/**
	 * Metodo que se invoca al producirse un evento sobre un boton
	 * Da el igual el boton que pulse que siempre se ejecuta
	 * Por lo tanto, el metodo debe diferenciar el boton pulsado
	 * Para ello hago uso de la propiedad ActionCommand que tiene
	 * cada boton. Accedo a ella mediante el metodo getActionComando
	 * del evento capturado. 
	 *
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("listener activado");
		
		String comando = e.getActionCommand();
		
		switch(comando) {
		
		case "NuevoCoche":{
			String matricula = vista.txtMatricula.getText();
			String modelo = vista.txtModelo.getText();
			double kms = Double.parseDouble(vista.txtKilometros.getText());
			LocalDate fechaFabricacion = vista.datePickerCoche.getDate();
			
			this.modelo.altaCoche(matricula, modelo, kms, fechaFabricacion);
		}
			break;
		case "EliminarCoche":{
			Coche cocheEliminado = vista.listCoches.getSelectedValue();
			modelo.eliminarCoche(cocheEliminado);

		}
			break;
		}
		
		refrescarLista();
	}
	
	/**
	 * Metodo para listar. Usa El DefaultListModel asociado a
	 * JList de coches para a�adir ah� los coches.
	 */
	private void refrescarLista() {
		vista.dlmCoches.clear();
		
		for(Coche coche : modelo.getCoches()) {
			vista.dlmCoches.addElement(coche);
		}
	}


}
