package base;

import java.time.LocalDate;

public class Coche {
	private String matricula;
	private String modelo;
	private double kms;
	private LocalDate fechaFabricacion;
	private Conductor conductor;
	
	public Coche(String matricula, String modelo, double kms, LocalDate fechaFabricacion) {
		super();
		this.matricula = matricula;
		this.modelo = modelo;
		this.kms = kms;
		this.fechaFabricacion = fechaFabricacion;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public double getKms() {
		return kms;
	}

	public void setKms(double kms) {
		this.kms = kms;
	}

	public LocalDate getFechaFabricacion() {
		return fechaFabricacion;
	}

	public void setFechaFabricacion(LocalDate fechaFabricacion) {
		this.fechaFabricacion = fechaFabricacion;
	}

	public Conductor getConductor() {
		return conductor;
	}

	public void setConductor(Conductor conductor) {
		this.conductor = conductor;
	}

	@Override
	public String toString() {
		return matricula + " " + modelo;
	}
	
	
}
