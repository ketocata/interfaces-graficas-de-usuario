package principal;

import mvc.Controlador;
import mvc.Modelo;
import mvc.Vista;

public class Principal {

	public static void main(String[] args) {
		
		Vista vista = new Vista();
		Modelo modelo = new Modelo();
		
		Controlador controlador = new Controlador(vista, modelo);
		

	}

}
