package datos;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class Modelo {
	
	private ArrayList<Alumno> listaAlumnos;
	
	public Modelo() {
		listaAlumnos = new ArrayList<Alumno>();
	}
	
	public void altaAlumno(String dni, String nombre, LocalDate fechaNacimiento) {

		Alumno alumno = new Alumno(dni, nombre, fechaNacimiento);
		listaAlumnos.add(alumno);
	}
	
	public void eliminarAlumno(String dniEliminar) {
		// Utilizo un iterador para eliminar el objeto
		Iterator<Alumno> iterador = listaAlumnos.iterator();
		while (iterador.hasNext()) {
			Alumno alumno = iterador.next();
			// Si coincide el dni, procedemos a eliminar
			if (alumno.getDni().equals(dniEliminar)) {
				iterador.remove();
			}
		}
	}

	public ArrayList<Alumno> getListaAlumnos() {
		return listaAlumnos;
	}

	
	@SuppressWarnings("unchecked")
	public void cargarDatos() throws ClassNotFoundException, IOException {
	
		FileInputStream flujoEntrada = new FileInputStream("datos.bin");
		ObjectInputStream deserializador = new ObjectInputStream(flujoEntrada);
		
		listaAlumnos = (ArrayList<Alumno>) deserializador.readObject();
		
		deserializador.close();
		
	}

	public void guardarDatos() throws IOException {
		
		FileOutputStream flujoSalida = new FileOutputStream("datos.bin");
		ObjectOutputStream serializador = new ObjectOutputStream(flujoSalida);
		
		serializador.writeObject(listaAlumnos);
		
		serializador.close();
		
	}
}
