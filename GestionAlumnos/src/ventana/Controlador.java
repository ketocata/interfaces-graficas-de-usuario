package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDate;

import javax.swing.JOptionPane;

import datos.Alumno;
import datos.Modelo;

public class Controlador implements ActionListener{

	private Modelo modelo;
	private Ventana vista;

	public Controlador(Modelo modelo, Ventana vista) {
		this.modelo = modelo;
		this.vista = vista;
		
		initActionHandlers(this);
	}

	/**
	 * Metodo que asocia los botones con la clase controlador, 
	 * la cual actua como listener
	 * @param listener el objeto de la clase controlador
	 */
	private void initActionHandlers(ActionListener listener) {
		// Boton de listar
		vista.btnListar.addActionListener(listener);

		// Boton de nuevo alumno
		vista.btnAlta.addActionListener(listener);

		// Boton de eliminar alumno
		vista.btnEliminar.addActionListener(listener);
		
		//Items del menu
		vista.menuItemCargar.addActionListener(listener);
		vista.menuItemGuardar.addActionListener(listener);
		vista.menuItemSalir.addActionListener(listener);
	}

	private void listarAlumnos() {
		vista.txtAreaListar.setText("");

		for (Alumno alumno : modelo.getListaAlumnos()) {
			vista.txtAreaListar.append(alumno + "\n");
		}
	}

	private void limpiarCampos() {
		vista.txtDni.setText("");
		vista.txtNombre.setText("");
		vista.datePicker.setText("");
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		//Obtengo el comando de accion para saber 
		//quien ha invocado el evento
		
		String comando = event.getActionCommand();
		
		switch(comando) {
			case "listarAlumnos":
				//Si el evento tiene el comando de listar
				listarAlumnos();
			break;
		
			case "altaAlumno":
				//Si el evento tiene el comando de nuevo alumno
				String dni = vista.txtDni.getText();
				String nombre = vista.txtNombre.getText();
				LocalDate fechaNacimiento = vista.datePicker.getDate();

				modelo.altaAlumno(dni, nombre, fechaNacimiento);

				limpiarCampos();
				listarAlumnos();
				
			break;
		
			case "eliminarAlumno":
				String dniEliminar = vista.txtDni.getText();

				modelo.eliminarAlumno(dniEliminar);

				limpiarCampos();
				listarAlumnos();
				
			break;
			
			case "Cargar":
				

			try {
				modelo.cargarDatos();
				
			} catch (ClassNotFoundException e1) {	
				
				e1.printStackTrace();
				
			} catch (IOException e2) {
				
				JOptionPane.showMessageDialog(null, "Error al leer fichero", "Error", JOptionPane.ERROR_MESSAGE);
				
				e2.printStackTrace();
			}

				
				break;
				
			case "Guardar":
				


			try {
				modelo.guardarDatos();
			} catch (IOException e) {
			
				
				
				e.printStackTrace();
			}


				
				break;
				
			case "Salir":
				
				System.exit(0);
				
				break;
		}
		
	}

}
