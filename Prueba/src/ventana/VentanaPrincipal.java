package ventana;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VentanaPrincipal extends JFrame {


	private static final long serialVersionUID = 1L;
	private JTextField txtNombre;
	private JButton btnAceptar;
	private JLabel lblNombre;
	private JCheckBox cbVisibilidad;
	private JLabel lblVisibleCB;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnVisible;
	private JRadioButton rdbtnInvisible;
	private JLabel lblVisibleRB;
	private JLabel lblContarCaracteres;
	private JTextField txtContarCaracteres;
	


	public VentanaPrincipal() {
		
		initComponents();
		initHandlers();
		
	}



	private void initHandlers() {
		btnAceptar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				String texto = txtNombre.getText();	
				System.out.println(texto);
				
				txtNombre.setText("");
				lblNombre.setText(texto);
				
			}
		});
		
		cbVisibilidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				lblVisibleCB.setVisible(!lblVisibleCB.isVisible());		
			}
		});
		
		
		rdbtnVisible.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				lblVisibleRB.setVisible(!rdbtnVisible.isVisible());
			}
		});
		
		rdbtnInvisible.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				lblVisibleRB.setVisible(!rdbtnInvisible.isVisible());
				
			}
		});
		
		txtContarCaracteres.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				
				String cadena = txtContarCaracteres.getText();
				int longitud = cadena.length();
				
				lblContarCaracteres.setText(longitud + " caracteres");
				
			}
		});
	}



	private void initComponents() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 539, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(45, 29, 70, 15);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 27, 193, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		btnAceptar = new JButton("Bien");
		btnAceptar.setBounds(385, 24, 117, 25);
		contentPane.add(btnAceptar);
		
		lblVisibleCB = new JLabel("Soy Visible");
		lblVisibleCB.setBounds(45, 71, 138, 15);
		contentPane.add(lblVisibleCB);
		
		cbVisibilidad = new JCheckBox("Hacer Visible\n");
		cbVisibilidad.setSelected(true);
		cbVisibilidad.setBounds(197, 67, 129, 23);
		contentPane.add(cbVisibilidad);
		
		rdbtnVisible = new JRadioButton("Visible");
		
		buttonGroup.add(rdbtnVisible);
		rdbtnVisible.setSelected(true);
		rdbtnVisible.setBounds(177, 109, 149, 23);
		contentPane.add(rdbtnVisible);
		
		rdbtnInvisible = new JRadioButton("Invisible");
		
		buttonGroup.add(rdbtnInvisible);
		rdbtnInvisible.setBounds(353, 109, 149, 23);
		contentPane.add(rdbtnInvisible);
		
		lblVisibleRB = new JLabel("Soy Visible");
		lblVisibleRB.setBounds(45, 113, 100, 15);
		contentPane.add(lblVisibleRB);
		
		lblContarCaracteres = new JLabel("0 caracteres");
		lblContarCaracteres.setBounds(45, 161, 138, 17);
		contentPane.add(lblContarCaracteres);
		
		txtContarCaracteres = new JTextField();
		
		txtContarCaracteres.setBounds(264, 159, 160, 19);
		
		contentPane.add(txtContarCaracteres);
		txtContarCaracteres.setColumns(10);
	}
}
